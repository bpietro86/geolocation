from django.db import models

class Download(models.Model):
    longitude = models.DecimalField(max_digits=9, decimal_places=6)
    latitude = models.DecimalField(max_digits=9, decimal_places=6)
    app_id = models.UUIDField()
    downloaded_at = models.DateTimeField()