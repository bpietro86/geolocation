import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

var socket = new WebSocket("ws://localhost:8000/chat/");

class App extends Component {

  constructor(props) {
    super(props)
    this.state = { message: '', messages: ['ciao'] }
    this.message = this.message.bind(this)
    this.send = this.send.bind(this)
    socket.onmessage = (e) => {
      this.setState({messages: [...this.state.messages, e.data], message: ''})
    }
  }
  
  message(e) {
    this.setState({message: e.target.value})
  }

  send() {   
    socket.send(this.state.message);
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div className="App-intro">
          <input type='text' onChange={this.message} value={this.state.message}/>
          <button onClick={this.send}>send</button>
        </div>
        <ul>
            { this.state.messages.map( (message, i) => <li key={`message-${i}`}>{message}</li>) }  
          </ul>
      </div>
    );
  }
}

export default App;
